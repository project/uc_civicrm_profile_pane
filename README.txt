
-- SUMMARY --

The Ubercart / Civicrm Profile Pane module allows an administrator to
choose any number of civicrm profiles to be displayed during ubercart
check-out as check-out panes.

For a full description of the module, visit the project page:
  http://drupal.org/project/uc_civicrm_profile_pane

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/uc_civicrm_profile_pane


-- REQUIREMENTS --

Ubercart, Civicrm


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- CONFIGURATION --

* Configure user permissions in Administer >> Profile Pane Settings
  - Administers will see a list of civicrm profiles (or a message
    alerting them that there are no currently configured profiles 
    that qualify for use as a check-out pane.  Check off the profiles
    that you would like included as check-out panes.

  - Administers will see a text box where you may give the checkout pane
    containing the selected profiles a title.  The default (if none is given)
    is 'Profile Pane Information'.

  Note that as of now, the existence of non-custom fields in a profile
  DISQUALIFIES it for inclusion via uc_civicrm_profile_panes into an
  ubercart check-out pane.  This is a feature that I hope to add in the future,
  OR feel free to submit a patch. :) 

-- CONTACT -- 

Current maintainers:
* A. J. Roach - http://drupal.org/user/35008

This project has been sponsored by:
* CIVICACTIONS, LLC

  Changing the world one node at a time!
